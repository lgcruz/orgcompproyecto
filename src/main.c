#include<stdio.h>
#include<stdlib.h>
void menu();
char* cryptAtbash(char* mensaje);
char* encryptCaesar(char* mensaje);
char* decryptCaesar(char* mensaje);
char* encryptD(char* mensaje,int despl);
char* decryptD(char* mensaje,int despl);
int main() {
	printf("Bienvenido!\n \t\t\tprograma codificador \n");
	char juego[50];
	int fin = 0;	
	while(fin != 1){
		char mensaje[100] = {0};
		char desp[20] = {0};
		int d = 0;
		menu();
		fgets(juego,50,stdin);
		if((char)juego[0] == '1'){
			printf("Ingrese el mensaje a codificar o decodificar:    (Atbash)\n");
			fgets(mensaje,100,stdin);
			char* code = cryptAtbash(mensaje);
			printf("mensaje codificado (%s)\n",code);
		}else if((char)juego[0] == '2'){
			printf("Ingrese el mensaje a codificar:    (Caesar)\n");
			fgets(mensaje,100,stdin);
			char* code = encryptCaesar(mensaje);
			printf("mensaje codificado (%s)\n",code);
		}else if((char)juego[0] == '3'){
			printf("Ingrese el mensaje a decodificar:  (Caesar)\n");
			fgets(mensaje,100,stdin);
			char* code = decryptCaesar(mensaje);
			printf("mensaje decodificado (%s)\n",code);
		}else if((char)juego[0] == '4'){
			printf("Ingrese el mensaje a codificar:    (Desplazamiento)\n");
			fgets(mensaje,100,stdin);
			printf("Ingrese el desplazamineto\n");
			fgets(desp,20,stdin);
			d = atoi(desp);
			char* code = encryptD(mensaje,d);
			printf("mensaje codificado (%s)\n",code);
		}else if((char)juego[0] == '5'){
			printf("Ingrese el mensaje a decodificar:  (Caesar)\n");
			fgets(mensaje,100,stdin);
			fgets(desp,20,stdin);
			d = atoi(desp);
			char* code = decryptD(mensaje,d);
			printf("mensaje decodificado (%s)\n",code);
		}else if((char)juego[0] == '6'){
			printf("Ha salido del programa\n");
			fin = 1;
		}else{
			printf("La opcion ingresada es incorrecta\n opciones=(1,2,3,4,5)\n");
		}
		printf("\n-----\n");
		
		
	}	
	return 0; 
}
void menu(){
	printf("\t----Menu----\n");
	printf("1.- Codificar o Decodificar mensaje   (Atbash)\n");
	printf("2.- Codificar mensaje   (Caesar)\n");
	printf("3.- Decodificar mensaje (Caesar)\n");
	printf("4.- Codificar mensaje   (Desplazamiento)\n");
	printf("5.- Decodificar mensaje (Desplazamiento)\n");
	printf("6.- Salir\n");
}
char* cryptAtbash(char* mensaje){
	int i=0;
	char codificado[100]={0};
	while(mensaje[i]!='\0'){
		if( ((int)mensaje[i]>=65) && ((int)mensaje[i]<=90) ){
			codificado[i] = 78 - (int)mensaje[i] + 77;
			
		}else if( ((int)mensaje[i]>=97) && ((int)mensaje[i]<=122) ){
			codificado[i] = 110 - (int)mensaje[i] + 109;
			
		}	
		printf("%s\n",codificado);
		i+=1;
	}
	char *p=codificado;
	return	p;
}
char* encryptCaesar(char* mensaje){
	int i=0;
	char codificado[100]={0};
	while(mensaje[i]!='\0'){
		if( ((int)mensaje[i]>=65) && ((int)mensaje[i]<=90) ){
			if((int)mensaje[i]+3 > 90){
				codificado[i] = (int)mensaje[i] + 3 - 26;
			}else{
				codificado[i] = (int)mensaje[i] + 3;
			}
		}else if( ((int)mensaje[i]>=97) && ((int)mensaje[i]<=122) ){
			if((int)mensaje[i]+3 > 122){
				codificado[i] = (int)mensaje[i] + 3 - 26;
			}else{
				codificado[i] = (int)mensaje[i] + 3;
			}
		}
		printf("%s\n",codificado);
		i+=1;
	}
	char *p=codificado;
	return	p;
}
char* decryptCaesar(char* mensaje){
	int i=0;
	char codificado[100]={0};
	while(mensaje[i]!='\0'){
		if( ((int)mensaje[i]>=65) && ((int)mensaje[i]<=90) ){
			if((int)mensaje[i]-3 < 65){
				codificado[i] = (int)mensaje[i] - 3 + 26;
			}else{
				codificado[i] = (int)mensaje[i] - 3;
			}
		}else if( ((int)mensaje[i]>=97) && ((int)mensaje[i]<=122) ){
			if((int)mensaje[i]-3 < 97){
				codificado[i] = (int)mensaje[i] - 3 + 26;
			}else{
				codificado[i] = (int)mensaje[i] - 3;
			}
		}
		printf("%s\n",codificado);
		i+=1;
	}
	char *p=codificado;
	return	p;
}
char* encryptD(char* mensaje,int despl){
	int i=0;
	char codificado[100]={0};
	while(mensaje[i]!='\0'){
		if( ((int)mensaje[i]>=65) && ((int)mensaje[i]<=90) ){
			if((int)mensaje[i]+despl > 90){
				codificado[i] = (int)mensaje[i] + despl - 26;
			}else{
				codificado[i] = (int)mensaje[i] + despl;
			}
		}else if( ((int)mensaje[i]>=97) && ((int)mensaje[i]<=122) ){
			if((int)mensaje[i]+despl > 122){
				codificado[i] = (int)mensaje[i] + despl - 26;
			}else{
				codificado[i] = (int)mensaje[i] + despl;
			}
		}
		printf("%s\n",codificado);
		i+=1;
	}
	char *p=codificado;
	return	p;
}
char* decryptD(char* mensaje,int despl){
	int i=0;
	char codificado[100]={0};
	while(mensaje[i]!='\0'){
		if( ((int)mensaje[i]>=65) && ((int)mensaje[i]<=90) ){
			if((int)mensaje[i]-despl < 65){
				codificado[i] = (int)mensaje[i] - despl + 26;
			}else{
				codificado[i] = (int)mensaje[i] - despl;
			}
		}else if( ((int)mensaje[i]>=97) && ((int)mensaje[i]<=122) ){
			if((int)mensaje[i]-despl < 97){
				codificado[i] = (int)mensaje[i] - despl + 26;
			}else{
				codificado[i] = (int)mensaje[i] - despl;
			}
		}
		printf("%s\n",codificado);
		i+=1;
	}
	char *p=codificado;
	return	p;
}

