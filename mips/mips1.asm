.data
	newLine:        .asciiz     "\n"
	menu: .asciiz "\t***Menu*** \n1. Codificar Mensaje\n2. Decodificar Mensaje\n3. Codificar Mensaje. CIFRADO CESAR\n4. Decodificar Mensaje. CIFRADO CESAR\n5. Salir\n"
	decryptMessage: .asciiz "You are decrypting a message\nIngrese el mensaje a decodificar:\n"
	encryptMessage: .asciiz "You are encrypting a message\nIngrese el mensaje a codificar:\n"
	outMessage: .asciiz "Usted ha salido del programa\n" 
	bienvenida: .asciiz "Bienvenido!\n \t\t\tprograma codificador \n"
	userMessage: .space 100
	caesarOut: .space 100
	caesarDecryptedOut: .space 100
	msgEncrypted: .asciiz "**Encryption Done**\nCypherText: "
	msgDecrypted: .asciiz  "**Decryption Done**\nDecypherText: "
	opcion: .space 20
	op1: .ascii "1"
	op2: .ascii "2"
	op3: .ascii "3"
	op4: .ascii "4"
	op5: .ascii "5"
.text

	main:
		li $v0, 4
		la $a0, bienvenida
		syscall
		
		while:
			# jump
			jal displayMenu
			
			# seleccion de opcion
			la      $s6, opcion
    			move    $t2, $s6
    			jal     getstr
			
			j while
		
		EXIT:
		
		
	li $v0, 4
	la $a0, outMessage
		
	#End of program
	li $v0, 10
	syscall	
	
	#PROMPT USER
	displayMenu:
		la      $a0, menu
    		li      $v0, 4
    		syscall
				
		jr $ra
		
	totalD:
		la 	$s0, userMessage     #s0 will hold message that will be iterated through
    		#lw 	$t1, key     #s1 will hold the key to shift by
    		li 	$t0, 0       #t0 will be iterator, starting at 0
		addi    $t4, $zero, 0 
		countLoop:
    			add 	$s1, $s0, $t0   #$s1 = message[i]
    			lb 	$s2, 0($s1)      #Loading char to shift into $s2
    			
    			la 	$t8 , newLine
    			lb 	$t8 , 0($t8)
    			beq 	$s2, $t8, end    #Breaking the loop if we've reached the end: http://stackoverflow.com/questions/12739463/how-to-iterate-a-string-in-mips-assembly
    			
    			add 	$t4, $t4, $s2    
			
			addi    $t0, $t0, 1  

    			j countLoop    #Going back to the beginning of the loop
    		end:

    		addi 	$a0, $t4, 0
		jr $ra
	
	encryptFunction:
		
		li 	$v0, 4
		la 	$a0, encryptMessage
		syscall	
			
		la 	$s4, userMessage	
		move    $a0, $s4
    		li      $a1, 99
    		li      $v0, 8
    		syscall
		
    		la 	$s0, userMessage     #s0 will hold message that will be iterated through
    		#lw 	$t1, key     #s1 will hold the key to shift by
    		jal 	totalD
    		addi	$t5, $a0, 0
    		    		
    		li 	$t0, 0       #t0 will be iterator, starting at 0
		addi    $t4, $zero, 122 
		encryptionLoop:
    			add 	$s1, $s0, $t0   #$s1 = message[i]
    			lb 	$s2, 0($s1)      #Loading char to shift into $s2
    			la 	$t8 , newLine
    			lb 	$t8 , 0($t8)
    			beq 	$s2, $t8, end1    #Breaking the loop if we've reached the end: http://stackoverflow.com/questions/12739463/how-to-iterate-a-string-in-mips-assembly
    			
    			mul 	$t5, $t5, $s2
    			   			
    			div 	$t5, $t5, 90
    			mfhi 	$t5
    			
    			add 	$s3, $s2, $t5    #Shifting the character by the key amount
    			slt	$t1, $s3, $t4
    			beq 	$t1, $zero, if
    			j else
    			if:
    				addi 	$s3, $s3, -91
    			else:
    			sb 	$s3, 0($s1)       #Changing the character in message to the shifted character
			
			addi    $t0, $t0, 1  
			
			li 	$v0, 4
			la 	$a0, userMessage
			syscall 
			
			li 	$v0, 4
			la 	$a0, newLine
			syscall 
		
    			j encryptionLoop    #Going back to the beginning of the loop
    		end1:
    		
    		li 	$v0, 4
		la 	$a0, newLine
		syscall
			
		jr $ra
		

	
	decryptFunction:
		li $v0, 4
		la $a0, decryptMessage
		syscall
		
		li $v0, 8
		la $a0, userMessage
		li $a1, 100 
		syscall 
		
		li $v0, 4
		la $a0, userMessage
		syscall
		
		jr $ra
		
	# Encryt and Decrypt Function implementing Caesar Cypher. 
	# Options 3 and 4. Men�
	
		caesarEncrypt:
			
		li 	$v0, 4
		la 	$a0, encryptMessage
		syscall	
			
		la 	$s4, userMessage	
		move    $a0, $s4
    		li      $a1, 99
    		li      $v0, 8
    		syscall
    		
    		li $t0, 0
    		la $s0, userMessage
    		la $s4, caesarOut
    		
    		caesarLoop:
    			
    			add 	$s1, $s0, $t0   
    			lb 	$s2, 0($s1)    
    			  
    			add 	$s5, $s4, $t0   
    			
    			la 	$t8 , newLine
    			lb 	$t8 , 0($t8)
    			beq 	$s2, $t8, endc
    			
    			addi 	$s3, $s2, 3 #Implements the key for the encrypting process
    			
    			sb 	$s3, 0($s5)       #Save the encrypted character in the output message	
			addi    $t0, $t0, 1  #Inncrements the variable of the loop
		
    			j caesarLoop    #Going back to the beginning of the loop
    		endc:
    		
    		li $v0, 4
    		la $a0, msgEncrypted
    		syscall
    		
    		li 	$v0, 4
		la 	$a0, caesarOut
		syscall 
    		
    		li 	$v0, 4
		la 	$a0, newLine
		syscall
			
		jr $ra
		
	caesarDecrypt:
			
		li 	$v0, 4
		la 	$a0, decryptMessage
		syscall	
			
		la 	$s4, userMessage	
		move    $a0, $s4
    		li      $a1, 99
    		li      $v0, 8
    		syscall
    		
    		li $t0, 0
    		la $s0, userMessage
    		la $s4, caesarDecryptedOut
    		
    		caesarLoop1:
    			
    			add 	$s1, $s0, $t0   
    			lb 	$s2, 0($s1)    
    			  
    			add 	$s5, $s4, $t0   
    			
    			la 	$t8 , newLine
    			lb 	$t8 , 0($t8)
    			beq 	$s2, $t8, endc1
    			
    			addi 	$s3, $s2, -3 #Implements the key for the decrypting process
    			
    			sb 	$s3, 0($s5)       #Save the decrypted character in the output message	
			addi    $t0, $t0, 1  #Inncrements the variable of the loop
		
    			j caesarLoop1    #Going back to the beginning of the loop
    		endc1:
    		
    		li $v0, 4
    		la $a0, msgDecrypted
    		syscall
    		
    		li 	$v0, 4
		la 	$a0, caesarDecryptedOut
		syscall 
    		
    		li 	$v0, 4
		la 	$a0, newLine
		syscall
			
		jr $ra	
    		
    		
		
	
		
	# getstr -- prompt and read string from user
	#
	# arguments:
	#   t2 -- address of string buffer
	getstr:
		addi 	$sp, $sp, -4
		sw 	$ra, 0($sp)
		
    		# read in the string
    		move    $a0, $t2
    		li      $a1, 19
    		li      $v0, 8
    		syscall

    		# deberiamos terminar? (op 3)
    		la      $a0, op5                     # obtener direccion de op3
    		lb      $a0, ($a0)                   # conseguir el contenido
    		lb      $t2, ($t2)                   # get first char of user string
    		beq     $t2, $a0, EXIT                # equal? yes, exit program
		
		# encriptamos (op1)
		la 	$a0, op1
		lb      $a0, ($a0)
		#lb      $t2, ($t2)
		beq     $t2, $a0, encryptFunction
		
		# deencriptamos (op2)
		la	$a0, op2
		lb      $a0, ($a0)
		#lb      $t2, ($t2)
		beq     $t2, $a0, decryptFunction

		#Caesar Cypher
		la 	$a0, op3
		lb      $a0, ($a0)
		#lb      $t2, ($t2)
		beq     $t2, $a0, caesarEncrypt
		
		#Caesar Decypher
		la 	$a0, op4
		lb      $a0, ($a0)
		#lb      $t2, ($t2)
		beq     $t2, $a0, caesarDecrypt
		
		lw 	$ra, 0($sp)
		addi	$sp, $sp, 4
		
    		jr      $ra                         # return
	
	
	
