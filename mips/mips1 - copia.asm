.data
	menu: .asciiz "***Menu*** \n1. Codificar Mensaje\n2. Decodificar Mensaje\n3. Salir\n INGRESE EL MENSAJE A CODIFICAR/DECODIFCAR SEGUIDO DE UN PUNTO FINAL.\n"
	decryptMessage: .asciiz "You are decrypting a message\nIngrese el mensaje a decodificar:\n"
	encryptMessage: .asciiz "You are encrypting a message\nIngrese el mensaje a codificar:\n"
	outMessage: .asciiz "Usted ha salido del programa\n" 
	userMessage: .space 100
	asciiCode: .byte
	mensajeSalida .space 100
.text

	main:
		jal displayMenu
		
		li $v0, 5 
		syscall
		
		move $t0, $v0

		#Options
		li $t1, 1
		li $t2, 2
		li $t3, 3
		
		#Condiciones
		beq $t0, $t1, encryptFunction
		beq $t0, $t2, decryptFunction
		beq $t0, $t3, EXIT
		
	displayMenu:
	
		li $v0, 4
		la $a0, menu
		syscall
		
		jr $ra
	
	encryptFunction:
		
		li $v0, 4
		la $a0, encryptMessage
		syscall	
			
		addi $sp, $sp, -4
		sw $ra, 0($sp)
		
		jal userInputFunction
		
		lw $ra, 0($sp)
		addi $sp, $sp, 4	
		
		#addi $sp, $sp, -400
			
		jr $ra
		
	decryptFunction:
		li $v0, 4
		la $a0, decryptMessage
		syscall
		
		addi $sp, $sp, -16
		sw $ra, 0($sp)
		
		jal userInputFunction
		
		li $v0, 4
		la $a0, userMessage
		syscall
		
		move $t0, $0 #Variable para el lazo 
		
		#Condici�n para que est� dentro del lazo 
		addi $t4, $0, 46
		li $v0,1
		addi $a0, $t4, 0 
		syscall		
		
		loop: 
			lb $t5, userMessage($t0)
			
			beq $t5, $t4, EXIT
			#slti $t5, $t0, 7
			#beq $t5, 0, EXIT

			#blt $t6, 32,  firstCondition
			#bgt $t6, 32,  firstCondition

			
			lb $a0, userMessage($t0)
			li $v0, 1 
			addi $t0, $t0, 1
			syscall
			
			addi $t5, $t5, -3
			li $v0, 1 
			addi $a0, $t5, 0
			syscall
			
			lw 
			sb $t5, $t0(	
			
			
			j loop	
			
		lw $ra, 0($sp)
		addi $sp, $sp, 4
		
		jr $ra

	firstCondition:
		#Hace lo del if, DECRYPT 
		li $v0, 1
		addi $a0, $t6, 0
		syscall 
		jr $ra
		
	secondCondition:

		#Hace lo del else. DECRYPT 
		li $v0, 1
		addi $a0, $t6, 0
		syscall 
		jr $ra
		
	EXIT:
		li $v0, 4
		la $a0, outMessage
		
		#End of program
		li $v0, 10
		syscall
	
	
	userInputFunction:
	
		li $v0, 8
		la $a0, userMessage
		li $a1, 100 
		syscall 
		
		li $v0, 4
		la $a0, userMessage
		syscall
		
		jr $ra
